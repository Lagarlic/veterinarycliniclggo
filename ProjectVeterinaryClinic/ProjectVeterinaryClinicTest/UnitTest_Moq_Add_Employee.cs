﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ProjectVeterinaryClinic;

namespace ProjectVeterinaryClinicTest
{
    [TestClass]
    public class UnitTest_Moq_Add_Employee
    {
        [TestMethod]
        public void Moq_Add_Employee()
        {
            Debug.WriteLine("Test Initialize ");

            var mockSet = new Mock<DbSet<Employee>>();
            var mockContext = new Mock<VeterinaryClinicLgGoEntities1>();

            mockContext.Setup(m => m.Employees).Returns(mockSet.Object);

            var staffDialog = new StaffDialog();
            staffDialog.addEmployee (mockContext.Object, "MOQ", "MOQ", new DateTime(1979, 1, 1), GenderEnumC.NA, "(565) 656-5656", "gilfanovola@gmail.com", "tbStreeText", "tbCityext", "tbStatText", "tbZiText", EmployeeTypeE.Other);

            //staffDialog.addEmployee("MOQ", "MOQ", new DateTime(1979, 1, 1), GenderEnumC.NA, "(565) huhutr656-5656", "gilfanovola@gmail.com", "tbStreeText", "tbCityext", "tbStatText", "tbZiText", EmployeeTypeE.Other);

            mockSet.Verify(m => m.Add(It.IsAny<Employee>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());


        }

    }
}
