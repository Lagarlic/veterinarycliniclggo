﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectVeterinaryClinic;
using System.Linq;

namespace ProjectVeterinaryClinicTest
{
    [TestClass]
    public class UnitTest1
    {
        VeterinaryClinicLgGoEntities1 ctx;
        [TestInitialize]
        public void TestInitialize()
        {
            Debug.WriteLine("Class Initialize ");
            ctx = new VeterinaryClinicLgGoEntities1();
            Debug.WriteLine("Test Initialize ");
        }
        [TestMethod]
        public void checkClientsFields()
        {
            var clients = (from c in ctx.Clients select c).ToList<Client>();
            foreach (var c in clients)
            {
                Assert.IsNotNull(c.ClientId);
                Assert.IsNotNull(c.FirstName);
                Assert.IsNotNull(c.LastName);
                Assert.IsNotNull(c.Gender);
                Assert.IsNotNull(c.DateOfBirth);
                Assert.IsNotNull(c.Street);
                Assert.IsNotNull(c.City);
                Assert.IsNotNull(c.State);
                Assert.IsNotNull(c.Zip);
                Assert.IsNotNull(c.Phone);
                //Assert.IsNotNull(c.Email);
            }
        }
        [TestMethod]
        public void checkEmployeesFields()
        {
            var employees = (from e in ctx.Employees select e).ToList<Employee>();
            foreach (var e in employees)
            {
                Assert.IsNotNull(e.EmployeeId);
                Assert.IsNotNull(e.FirstName);
                Assert.IsNotNull(e.LastName);
                Assert.IsNotNull(e.Gender);
                Assert.IsNotNull(e.DateOfBirth);
                Assert.IsNotNull(e.Street);
                Assert.IsNotNull(e.City);
                Assert.IsNotNull(e.State);
                Assert.IsNotNull(e.Zip);
                Assert.IsNotNull(e.Phone);
                //Assert.IsNotNull(e.Email);
                Assert.IsNotNull(e.EmployeeType);
            }
        }
        [TestMethod]
        public void checkSchedulesFields()
        {
            var schedules = (from s in ctx.Schedules select s).ToList<Schedule>();
            foreach (var s in schedules)
            {
                Assert.IsNotNull(s.ScheduleId);
                Assert.IsNotNull(s.ClientId);
                Assert.IsNotNull(s.PetId);
                Assert.IsNotNull(s.EmployeeId);
                Assert.IsNotNull(s.Task);
                Assert.IsNotNull(s.Status);
                Assert.IsNotNull(s.DateTime);
            }
        }
        [TestMethod]
        public void checkPetsFields()
        {
            var pets = (from p in ctx.Pets select p).ToList<Pet>();
            foreach (var p in pets)
            {
                Assert.IsNotNull(p.PetId);
                Assert.IsNotNull(p.ClientId);
                Assert.IsNotNull(p.Gender);
                Assert.IsNotNull(p.Type);
            }
        }
        [TestMethod]
        public void checkClientFieldValidationMethods()
        {
            Assert.IsFalse(ProjectVeterinaryClinic.Client.isFirstNameValid(";@5"));
            Assert.IsTrue(ProjectVeterinaryClinic.Client.isFirstNameValid("Anna"));
            Assert.IsFalse(ProjectVeterinaryClinic.Client.isLastNameValid("sssssssssssssssssssssssskldjjjjjjjjjjjlfggggggggggggggggggggggg"));
            Assert.IsTrue(ProjectVeterinaryClinic.Client.isLastNameValid("Iv"));
            Assert.IsFalse(ProjectVeterinaryClinic.Client.isDateOfBirthValid(DateTime.Now));
            Assert.IsTrue(ProjectVeterinaryClinic.Client.isDateOfBirthValid(DateTime.Today.AddYears(-20)));
            Assert.IsFalse(ProjectVeterinaryClinic.Client.isPhoneValid(""));
            Assert.IsTrue(ProjectVeterinaryClinic.Client.isPhoneValid("(438) 222-2222"));
        }
        [TestMethod]
        public void checkEmployeeFieldValidationMethods()
        {
            Assert.IsFalse(ProjectVeterinaryClinic.Client.isFirstNameValid(""));
            Assert.IsTrue(ProjectVeterinaryClinic.Client.isFirstNameValid("Tommy"));
            Assert.IsFalse(ProjectVeterinaryClinic.Client.isLastNameValid("%^&$#%%^"));
            Assert.IsTrue(ProjectVeterinaryClinic.Client.isLastNameValid("Popov"));
            Assert.IsFalse(ProjectVeterinaryClinic.Client.isDateOfBirthValid(DateTime.Now));
            Assert.IsTrue(ProjectVeterinaryClinic.Client.isDateOfBirthValid(DateTime.Today.AddYears(-19)));
            Assert.IsFalse(ProjectVeterinaryClinic.Client.isPhoneValid("dfgggghghhhhhhhhhhhhhhff"));
            Assert.IsTrue(ProjectVeterinaryClinic.Client.isPhoneValid("(438) 222-2222"));
        }
    }
}
