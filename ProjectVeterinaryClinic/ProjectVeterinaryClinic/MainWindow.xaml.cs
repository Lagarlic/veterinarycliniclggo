﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectVeterinaryClinic
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btClients_Click(object sender, RoutedEventArgs e)
        {
            ClientsDialog clientsDialog = new ClientsDialog();
            if (clientsDialog.ShowDialog() == true) { }
        }

        private void btWeeklyTimetable_Click(object sender, RoutedEventArgs e)
        {
            WeeklyTimetableDialog weeklyTimetableDialog = new WeeklyTimetableDialog();
            if (weeklyTimetableDialog.ShowDialog() == true) { }
        }

        private void btStaff_Click(object sender, RoutedEventArgs e)
        {
            StaffDialog staffDialog = new StaffDialog();
            if (staffDialog.ShowDialog() == true) { }
        }

        private void btStatistics_Click(object sender, RoutedEventArgs e)
        {
            StatisticsDialog statisticsDialog = new StatisticsDialog();
            if (statisticsDialog.ShowDialog() == true) { }
        }

        private void btSchedule_Click(object sender, RoutedEventArgs e)
        {
            ScheduleDialog scheduleDialog = new ScheduleDialog();
            if (scheduleDialog.ShowDialog() == true) { }
        }

        private void btMailing_Click(object sender, RoutedEventArgs e)
        {
            MailingDialog mailing = new MailingDialog();
            if (mailing.ShowDialog() == true) { }
        }
    }
}
