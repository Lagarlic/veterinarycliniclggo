﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectVeterinaryClinic
{
    public class Globals
    {
        private static VeterinaryClinicLgGoEntities1 _ctx;
        public static VeterinaryClinicLgGoEntities1 Ctx // ex System Exception
        {
            get
            {
                if (_ctx == null)
                {
                    _ctx = new VeterinaryClinicLgGoEntities1();
                }
                return _ctx;
            }
        }

        public enum GenterEnum1 { Male = 1, Female = 2, NA = 3}
    }
}
