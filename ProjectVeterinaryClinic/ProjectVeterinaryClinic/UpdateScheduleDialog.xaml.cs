﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectVeterinaryClinic
{
    /// <summary>
    /// Interaction logic for UpdateScheduleDialog.xaml
    /// </summary>
    public partial class UpdateScheduleDialog : Window
    {
        Schedule currSchedule;
        public UpdateScheduleDialog( Schedule s)
        {
            InitializeComponent();
            currSchedule = s;
            lblDate.Content = currSchedule.DateTime;
            lblDocName.Content = currSchedule.DocFullName;
            lblClientName.Content = currSchedule.ClientFullName;
            lblClientPhone.Content = currSchedule.ClientPhone;
            lblPetTitle.Content = currSchedule.Pet.Title;
            lblPetType.Content = currSchedule.Pet.Type;
            lblPetDoB.Content = currSchedule.Pet.DateOfBirth;
            lblPetGender.Content = currSchedule.Pet.Gender;
            if (currSchedule.Status == ScheduleStatusEnum.Done)
            {
                rbStatusDone.IsChecked = true;
            }
            else if (currSchedule.Status == ScheduleStatusEnum.Pending)
            {
                rbStatusPending.IsChecked = true;
            }
            else if(currSchedule.Status == ScheduleStatusEnum.Canceled)
            {
                rbStatusCanceled.IsChecked = true;
            }
            else if(currSchedule.Status == ScheduleStatusEnum.Other)
            {
                rbStatusOther.IsChecked = true;
            }
            else
            { // internal error
                MessageBox.Show(this, "Error reading Status value", "Data error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            tbTask.Text = currSchedule.Task;
            tbNote.Text = currSchedule.Note;
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {            
            if (currSchedule != null)
            {
                int currScheduleId = currSchedule.ScheduleId;
                try
                {
                    using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                    {
                        Schedule scheduleToUpdate = (from s in ctx.Schedules where s.ScheduleId == currScheduleId select s).FirstOrDefault<Schedule>();
                        if (scheduleToUpdate != null)
                        {
                            scheduleToUpdate.Task = tbTask.Text;
                            scheduleToUpdate.Note = tbNote.Text;                          
                            if (rbStatusCanceled.IsChecked == true)
                            {
                                scheduleToUpdate.Status = ScheduleStatusEnum.Canceled;
                            }
                            else if (rbStatusDone.IsChecked == true)
                            {
                                scheduleToUpdate.Status = ScheduleStatusEnum.Done;
                            }
                            else if (rbStatusPending.IsChecked == true)
                            {
                                scheduleToUpdate.Status = ScheduleStatusEnum.Pending;
                            }
                            else if (rbStatusOther.IsChecked == true)
                            {
                                scheduleToUpdate.Status = ScheduleStatusEnum.Other;
                            }
                            else
                            { // internal error
                                MessageBox.Show(this, "Error reading radio buttons state", "Internal error", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }                            
                                ctx.SaveChanges();
                                //MessageBox.Show("Record updated.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);                                                          
                        }
                        else
                        {
                            MessageBox.Show("Record to update not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                this.DialogResult = true;
            }
        }
    }
}
