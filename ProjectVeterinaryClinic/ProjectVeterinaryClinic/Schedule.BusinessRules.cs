﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectVeterinaryClinic
{
    partial class Schedule
    {
        public string DocFullName { get { return string.Format("{0} {1}", Employee?.FirstName, Employee?.LastName); } }
        public string ClientFullName { get { return string.Format("{0} {1}", Client?.FirstName, Client?.LastName); } }
        public string PetType { get { return Pet?.Type.ToString(); } }
        public string ClientPhone { get { return Client?.Phone;  } }        
    }
}
