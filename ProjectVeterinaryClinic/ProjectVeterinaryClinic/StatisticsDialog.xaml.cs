﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectVeterinaryClinic
{
    /// <summary>
    /// Interaction logic for StatisticsDialog.xaml
    /// </summary>
     public partial class StatisticsDialog : Window
    {
        public StatisticsDialog()
        {
            InitializeComponent();
            showPieChart();
            showBarChart();
            showColumnChart();
        }

        
        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;            
        }
        private void showPieChart()
        {
            List<KeyValuePair<string, int>> valueList = new List<KeyValuePair<string, int>>();
            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1()) 
            {
                try
                {
                    valueList.Add(new KeyValuePair<string, int>("Dog", (from p in ctx.Pets where p.Type == PetsTypeEnum.Dog select p).Count()));
                    valueList.Add(new KeyValuePair<string, int>("Cat", (from p in ctx.Pets where p.Type == PetsTypeEnum.Cat select p).Count()));
                    valueList.Add(new KeyValuePair<string, int>("Rodent", (from p in ctx.Pets where p.Type == PetsTypeEnum.Rodent select p).Count()));
                    valueList.Add(new KeyValuePair<string, int>("Reptile", (from p in ctx.Pets where p.Type == PetsTypeEnum.Reptile select p).Count()));
                    valueList.Add(new KeyValuePair<string, int>("Fish", (from p in ctx.Pets where p.Type == PetsTypeEnum.Fish select p).Count()));
                    valueList.Add(new KeyValuePair<string, int>("Insect", (from p in ctx.Pets where p.Type == PetsTypeEnum.Insect select p).Count()));
                    valueList.Add(new KeyValuePair<string, int>("N/A", (from p in ctx.Pets where p.Type == PetsTypeEnum.NA select p).Count()));
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            //Setting data for column chart
            columnChart.DataContext = valueList;

            // Setting data for pie chart
            pieChart.DataContext = valueList;
        }

        private void showBarChart()
        {
            List<KeyValuePair<string, int>> valueList = new List<KeyValuePair<string, int>>();
            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
            {
                try
                {
                    valueList.Add(new KeyValuePair<string, int>("Director", (from e in ctx.Employees where e.EmployeeType == EmployeeTypeE.Director select e).Count()));
                    valueList.Add(new KeyValuePair<string, int>("Doctor", (from e in ctx.Employees where e.EmployeeType == EmployeeTypeE.Doctor select e).Count()));
                    valueList.Add(new KeyValuePair<string, int>("Assistant", (from e in ctx.Employees where e.EmployeeType == EmployeeTypeE.Assistant select e).Count()));
                    valueList.Add(new KeyValuePair<string, int>("Secretary", (from e in ctx.Employees where e.EmployeeType == EmployeeTypeE.Secretary select e).Count()));
                    valueList.Add(new KeyValuePair<string, int>("Other", (from e in ctx.Employees where e.EmployeeType == EmployeeTypeE.Other select e).Count()));                    
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            //Setting data for bar chart
            barChart.DataContext = valueList;
        }

        private void showColumnChart()
        {
            List<KeyValuePair<string, int>> valueList = new List<KeyValuePair<string, int>>();
            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
            {
                try
                {
                    valueList.Add(new KeyValuePair<string, int>("JAN", ctx.Schedules.Where( s => s.DateTime.Month == 1 && s.DateTime.Year == 2020 ).Count()));                    
                    valueList.Add(new KeyValuePair<string, int>("FEB", ctx.Schedules.Where(s => s.DateTime.Month == 2 && s.DateTime.Year == 2020).Count()));
                    valueList.Add(new KeyValuePair<string, int>("MAR", ctx.Schedules.Where(s => s.DateTime.Month == 3 && s.DateTime.Year == 2020).Count()));
                    valueList.Add(new KeyValuePair<string, int>("APR", ctx.Schedules.Where(s => s.DateTime.Month == 4 && s.DateTime.Year == 2020).Count()));
                    valueList.Add(new KeyValuePair<string, int>("MAY", ctx.Schedules.Where(s => s.DateTime.Month == 5 && s.DateTime.Year == 2020).Count()));
                    valueList.Add(new KeyValuePair<string, int>("JUN", ctx.Schedules.Where(s => s.DateTime.Month == 6 && s.DateTime.Year == 2020).Count()));
                    valueList.Add(new KeyValuePair<string, int>("JUL", ctx.Schedules.Where(s => s.DateTime.Month == 7 && s.DateTime.Year == 2020).Count()));
                    valueList.Add(new KeyValuePair<string, int>("AUG", ctx.Schedules.Where(s => s.DateTime.Month == 8 && s.DateTime.Year == 2020).Count()));
                    valueList.Add(new KeyValuePair<string, int>("SEP", ctx.Schedules.Where(s => s.DateTime.Month == 9 && s.DateTime.Year == 2020).Count()));
                    valueList.Add(new KeyValuePair<string, int>("OCT", ctx.Schedules.Where(s => s.DateTime.Month == 10 && s.DateTime.Year == 2020).Count()));
                    valueList.Add(new KeyValuePair<string, int>("NOV", ctx.Schedules.Where(s => s.DateTime.Month == 11 && s.DateTime.Year == 2020).Count()));
                    valueList.Add(new KeyValuePair<string, int>("DEC", ctx.Schedules.Where(s => s.DateTime.Month == 12 && s.DateTime.Year == 2020).Count()));
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            //Setting data for column chart
            columnChart.DataContext = valueList;
        }
    }
}
