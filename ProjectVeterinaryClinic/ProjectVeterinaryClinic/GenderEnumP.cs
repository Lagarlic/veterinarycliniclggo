//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectVeterinaryClinic
{
    using System;
    
    public enum GenderEnumP : int
    {
        Male = 1,
        Female = 2,
        NA = 3
    }
}
