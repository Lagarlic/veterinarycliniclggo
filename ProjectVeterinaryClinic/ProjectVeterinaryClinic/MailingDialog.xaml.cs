﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Mail;
using System.Net;

namespace ProjectVeterinaryClinic
{
    /// <summary>
    /// Interaction logic for MailingDialog.xaml
    /// </summary>
    public partial class MailingDialog : Window
    {
        public MailingDialog()
        {
            InitializeComponent();
        }
        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            bool flag = false;

            if (txtSubject.Text.Trim().Length == 0)
            {
                flag = false;
                txtbSubject.Text = "Required";
                txtSubject.Focus();
            }
            if (txtContent.Text.Trim().Length == 0)
            {
                flag = false;
                txtbContent.Text = "Required";
                txtContent.Focus();
            }
            if (rbOneClient.IsChecked == true)
            {
                if (txtTo.Text.Trim().Length == 0)
                {
                    flag = false;
                    txtbTo.Text = "Required";
                    txtTo.Focus();
                }
                else if (!Regex.IsMatch(txtTo.Text, @"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}"))
                {
                    flag = false;
                    txtbTo.Text = "Invalid";
                    txtTo.Focus();
                }
                else
                {
                    flag = true;
                    txtbTo.Text = "";
                }
                try
                {
                    sendEmail(flag, txtTo.Text.Trim());                   
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(this, ex.Message, "Email sending error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else if (rbAllClients.IsChecked == true)
            {
                flag = true;
                try
                {
                    using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                    {
                        var clients = (from c in ctx.Clients select c).ToList<Client>();
                        foreach (Client c in clients)
                        {
                            if (c.Email != "")
                            {
                                
                                    sendEmail(flag, c.Email);                                
                            }
                        }
                    }                    
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void sendEmail(bool flag, string emailAddressTo)
        {
            if (flag)
            {
                var smtpServerName = ConfigurationManager.AppSettings["SmtpServer"];
                var port = ConfigurationManager.AppSettings["Port"];
                var senderEmailId = ConfigurationManager.AppSettings["SenderEmailId"];
                var senderPassword = ConfigurationManager.AppSettings["SenderPassword"];

                var smptClient = new SmtpClient(smtpServerName, Convert.ToInt32(port))
                {
                    Credentials = new NetworkCredential(senderEmailId, senderPassword),
                    EnableSsl = true
                };
                try { 
                smptClient.Send(senderEmailId, emailAddressTo, txtSubject.Text, txtContent.Text);                
                MessageBox.Show("Message Sent Successfully");
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                resetAllFields();
            }
        }
        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            resetAllFields();
        }

        void resetAllFields()
        {
            txtTo.Text = "";
            txtSubject.Text = "";
            txtContent.Text = "";
            txtTo.Focus();
            rbOneClient.IsChecked = true;
            txtbTo.Text = "";
            txtbSubject.Text = "";
            txtbContent.Text = "";
        }

            private void rbAllClients_Checked(object sender, RoutedEventArgs e)
        {
            if (rbAllClients.IsChecked == true)
            {
                txtTo.Text = "To All Clients";
                txtTo.IsEnabled = false;
            }

        }

        private void rbOneClient_Checked(object sender, RoutedEventArgs e)
        {
            if (rbOneClient.IsChecked == true)
            {
                txtTo.Text = "";
                txtTo.IsEnabled = true;
            }
        }
    }
}
