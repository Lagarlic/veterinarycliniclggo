//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectVeterinaryClinic
{
    using System;
    
    public enum PetsTypeEnum : int
    {
        Dog = 1,
        Cat = 2,
        Rodent = 3,
        Reptile = 4,
        Fish = 5,
        Insect = 6,
        NA = 7
    }
}
