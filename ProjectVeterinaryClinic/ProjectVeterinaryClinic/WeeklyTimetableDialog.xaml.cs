﻿using Syncfusion.UI.Xaml.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectVeterinaryClinic
{
    /// <summary>
    /// Interaction logic for WeeklyTimetableDialog.xaml
    /// </summary>
    public partial class WeeklyTimetableDialog : Window //, Control, ISchedulerComponentOwner, INotifyDependencyPropertyChanged
    {
        SfSchedule schedule = new SfSchedule();       
        public WeeklyTimetableDialog()
        {
            InitializeComponent();
            this.Content = schedule;
            schedule.ScheduleType = ScheduleType.WorkWeek;
            schedule.TimeInterval = TimeInterval.TwentyMin;
            schedule.FirstDayOfWeek = DayOfWeek.Monday;
            schedule.WorkStartHour = 9 ;
            schedule.WorkEndHour = 18;
            schedule.ShowNonWorkingHours = false;
            schedule.AllowAppointmentEditing = false;
            schedule.AllowAddNewAppointment = false;
            addingAppointments();
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void addingAppointments()
        {
            ScheduleAppointmentCollection appointmentCollection = new ScheduleAppointmentCollection();
            //
            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
            {
                var schedulesListFromDB = (from s in ctx.Schedules.Include("Employee").Include("Pet").Include("Client") select s).ToList<Schedule>();
                foreach (Schedule s in schedulesListFromDB)
                {
                    ScheduleAppointment clientMeeting = new ScheduleAppointment();
                    DateTime startTime = s.DateTime;
                    DateTime endTime = startTime.AddMinutes(20);
                    clientMeeting.StartTime = startTime;
                    clientMeeting.EndTime = endTime;
                    clientMeeting.Subject = s.Employee.FirstName + s.Employee.LastName;
                    appointmentCollection.Add(clientMeeting);
                }
            }            
            schedule.Appointments = appointmentCollection;
        }
    }
}
