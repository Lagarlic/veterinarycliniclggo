﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectVeterinaryClinic
{
    /// <summary>
    /// Interaction logic for ClientsDialog.xaml
    /// </summary>
    public partial class ClientsDialog : Window
    {
        public ClientsDialog()
        {
            InitializeComponent(); //TODO disable/enable Buttons
            updateListView();
            lblAgeErrorMessage.Visibility = Visibility.Hidden;
            lblFNameErrorMessage.Visibility = Visibility.Hidden;
            lblLEmailErrorMessage.Visibility = Visibility.Hidden;
            lblLNameErrorMessage.Visibility = Visibility.Hidden;
            lblPhoneErrorMessage.Visibility = Visibility.Hidden;

            btDelete.IsEnabled = false;
            btUpdate.IsEnabled = false;
            btManagePets.IsEnabled = false;
        }

        void updateListView()
        {
            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    var clients = (from c in ctx.Clients.Include("Pets") select c).ToList<Client>();
                    //Globals.Ctx.SaveChanges();
                    lvClients.ItemsSource = clients;
                    lvClients.Items.Refresh();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void resetAllFields()
        {
            tbFName.Text = "";
            tbLName.Text = "";
            dpDateOfBirth.SelectedDate = null;
            rbGenderNA.IsChecked = true;
            tbPhone.Text = "";
            tbEmail.Text = "";
            tbStreet.Text = "";
            tbCity.Text = "";
            tbState.Text = "";
            tbZip.Text = "";
            lvClients.SelectedIndex = -1;
            tbSearch.Text = "";
            //----------------------------
            lblAgeErrorMessage.Visibility = Visibility.Hidden;
            lblLNameErrorMessage.Visibility = Visibility.Hidden;
            lblFNameErrorMessage.Visibility = Visibility.Hidden;
            lblPhoneErrorMessage.Visibility = Visibility.Hidden;
            lblLEmailErrorMessage.Visibility = Visibility.Hidden;
            btDelete.IsEnabled = false;
            btUpdate.IsEnabled = false;
            btManagePets.IsEnabled = false;
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string firstName = tbFName.Text;
            string lastName = tbLName.Text;
            if (dpDateOfBirth.SelectedDate == null)
            {
                lblAgeErrorMessage.Visibility = Visibility.Visible;
                lblAgeErrorMessage.Content = "Please select the Date.";
                return;
            }
            else
            {
                lblAgeErrorMessage.Visibility = Visibility.Hidden;
            }

            DateTime dateOfBirth = (DateTime)dpDateOfBirth.SelectedDate; // FIXME null point ex if nothing selected
            GenderEnumC gender;
            if (rbGenderMale.IsChecked == true)
            {
                gender = GenderEnumC.Male;
            }
            else if (rbGenderFemale.IsChecked == true)
            {
                gender = GenderEnumC.Female;
            }
            else if (rbGenderNA.IsChecked == true)
            {
                gender = GenderEnumC.NA;
            }
            else
            { // internal error
                MessageBox.Show(this, "Error reading radio buttons state", "Internal error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string phone = tbPhone.Text;
            string email = tbEmail.Text;
            string street = tbStreet.Text;
            string city = tbCity.Text;
            string state = tbState.Text;
            string zip = tbZip.Text;
            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
            {
            ctx.Clients.Add(new Client
                {
                    FirstName = firstName,
                    LastName = lastName,
                    DateOfBirth = dateOfBirth,
                    Gender = gender,
                    Phone = phone,
                    Email = email,
                    Street = street,
                    City = city,
                    State = state,
                    Zip = zip
                });
                try
                {
                ctx.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {                
                string errMessage = "";
                    foreach (var error in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in error.ValidationErrors)
                        {
                            errMessage += validationError.ErrorMessage + "\n";
                        }
                    }
                    MessageBox.Show(this, errMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            resetAllFields();               
            updateListView();
        }

        Client selectedClient;
        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            selectedClient = (Client)lvClients.SelectedItem;            
            if (selectedClient != null)
            {
                int selectedId = selectedClient.ClientId;
                MessageBoxResult result = MessageBox.Show(this, "Do you want to delete selected client?", "Program message", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        try
                        {
                            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                            {
                                Client clientToDelete = (from c in ctx.Clients where c.ClientId == selectedId select c).FirstOrDefault<Client>();
                                if (clientToDelete != null)
                                {


                                    ctx.Clients.Remove(clientToDelete); // schedule for deletion from database
                                    ctx.SaveChanges();
                                    //MessageBox.Show("Record deleted.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                    updateListView();
                                    resetAllFields();
                                }
                                else
                                {
                                    MessageBox.Show("Record to delete not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                            }
                        }
                        catch (SystemException ex)
                        {
                            MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        Console.WriteLine("This shouldn't happen!");
                        return;
                }
            }
        }

        private void lvClients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedClient = (Client)lvClients.SelectedItem;
            if (selectedClient != null)
            {
                tbFName.Text = selectedClient.FirstName;
                tbLName.Text = selectedClient.LastName;
                dpDateOfBirth.SelectedDate = selectedClient.DateOfBirth;
                if (selectedClient.Gender == GenderEnumC.Male)
                {
                    rbGenderMale.IsChecked = true;
                }
                else if (selectedClient.Gender == GenderEnumC.Female)
                {
                    rbGenderFemale.IsChecked = true;
                }
                else if (selectedClient.Gender == GenderEnumC.NA)
                {
                    rbGenderNA.IsChecked = true;
                }
                else
                { // internal error
                    MessageBox.Show(this, "Error reading gender value", "Data error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                tbPhone.Text = selectedClient.Phone;
                tbEmail.Text = selectedClient.Email;
                tbStreet.Text = selectedClient.Street;
                tbCity.Text = selectedClient.City;
                tbState.Text = selectedClient.State;
                tbZip.Text = selectedClient.Zip;
                //-----------------------------------------------
                lblAgeErrorMessage.Visibility = Visibility.Hidden;
                lblLNameErrorMessage.Visibility = Visibility.Hidden;
                lblFNameErrorMessage.Visibility = Visibility.Hidden;
                lblPhoneErrorMessage.Visibility = Visibility.Hidden;
                lblLEmailErrorMessage.Visibility = Visibility.Hidden;
                btDelete.IsEnabled = true;
                btUpdate.IsEnabled = true;
                btManagePets.IsEnabled = true;
            }
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            selectedClient = (Client)lvClients.SelectedItem;
            if (selectedClient != null)
            {
                int selectedId = selectedClient.ClientId;
                try
                {
                    using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                    {
                        Client clientToUpdate = (from c in ctx.Clients where c.ClientId == selectedId select c).FirstOrDefault<Client>();
                        if (clientToUpdate != null)
                        {
                            clientToUpdate.FirstName = tbFName.Text;
                            clientToUpdate.LastName = tbLName.Text;
                            if (dpDateOfBirth.SelectedDate == null)
                            {
                                lblAgeErrorMessage.Visibility = Visibility.Visible;
                                lblAgeErrorMessage.Content = "Please select the Date.";
                                return;
                            }
                            clientToUpdate.DateOfBirth = (DateTime)dpDateOfBirth.SelectedDate;  
                            
                            if (rbGenderMale.IsChecked == true)
                            {
                                clientToUpdate.Gender = GenderEnumC.Male;
                            }
                            else if (rbGenderFemale.IsChecked == true)
                            {
                                clientToUpdate.Gender = GenderEnumC.Female;
                            }
                            else if (rbGenderNA.IsChecked == true)
                            {
                                clientToUpdate.Gender = GenderEnumC.NA;
                            }
                            else
                            { // internal error
                                MessageBox.Show(this, "Error reading radio buttons state", "Internal error", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                            clientToUpdate.Phone = tbPhone.Text;
                            clientToUpdate.Email = tbEmail.Text;
                            clientToUpdate.Street = tbStreet.Text;
                            clientToUpdate.City = tbCity.Text;
                            clientToUpdate.State = tbState.Text;
                            clientToUpdate.Zip = tbZip.Text;
                            try
                            {
                            ctx.SaveChanges();
                                //MessageBox.Show("Record updated.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                updateListView();
                                resetAllFields();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                string errMessage = "";
                                foreach (var error in ex.EntityValidationErrors)
                                {
                                    foreach (var validationError in error.ValidationErrors)
                                    {
                                        errMessage += validationError.ErrorMessage + "\n";
                                    }
                                }
                                MessageBox.Show(this, errMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Record to update not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }                    
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }            
        }

        private void btManagePets_Click(object sender, RoutedEventArgs e)
        {
            selectedClient = (Client)lvClients.SelectedItem;
            if (selectedClient != null)
            {
                PetsDialog petsDialog = new PetsDialog(selectedClient);
                if (petsDialog.ShowDialog() == true)
                {
                    updateListView();
                }
            }
        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            resetAllFields();
        }

        private void tbFName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!Client.isFirstNameValid(tbFName.Text))
            {
                lblFNameErrorMessage.Visibility = Visibility.Visible;
            }
            else lblFNameErrorMessage.Visibility = Visibility.Hidden;
        }

        private void tbLName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!Client.isLastNameValid(tbLName.Text))
            {
                lblLNameErrorMessage.Visibility = Visibility.Visible;
            }
            else lblLNameErrorMessage.Visibility = Visibility.Hidden;
        }

        private void dpDateOfBirth_LostFocus(object sender, RoutedEventArgs e)
        {
            if (dpDateOfBirth.SelectedDate == null)
            {
                lblAgeErrorMessage.Visibility = Visibility.Visible;                               
            }
            else if (dpDateOfBirth.SelectedDate != null)
            {                
                if (!Client.isDateOfBirthValid((DateTime)dpDateOfBirth.SelectedDate))
                {
                    lblAgeErrorMessage.Visibility = Visibility.Visible;
                }
                else lblAgeErrorMessage.Visibility = Visibility.Hidden;
            }
        }

        private void tbPhone_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!Client.isPhoneValid(tbPhone.Text))
            {
                lblPhoneErrorMessage.Visibility = Visibility.Visible;
            }
            else lblPhoneErrorMessage.Visibility = Visibility.Hidden;
        }

        private void tbEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!Client.isEmailValid(tbEmail.Text))
            {
                lblLEmailErrorMessage.Visibility = Visibility.Visible;
            }
            else lblLEmailErrorMessage.Visibility = Visibility.Hidden;
        }
        // filter
        private void TbSearch_TextChanged(object sender, TextChangedEventArgs e)

        {
            string text = tbSearch.Text;
            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    if (text != "")
                    {

                        var clients = (from c in ctx.Clients.Include("Pets") select c).ToList<Client>();
                        ctx.SaveChanges();
                        List<Client> clientsToShow = new List<Client>();
                        foreach (Client c in clients)
                        {
                            if (c.ToStringForSorting().Contains(text))
                            {
                                clientsToShow.Add(c);
                            }
                        }
                        lvClients.ItemsSource = clientsToShow;
                        lvClients.Items.Refresh();
                    }
                    else if (text == "")
                    {
                        updateListView();
                    }

                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        // sorting
        void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;            
            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    switch (headerClicked.Content)
                    {

                        case "Client Full Name":
                            var clients = (from c in ctx.Clients.Include("Pets") orderby c.LastName.ToString() select c).ToList<Client>();
                            ctx.SaveChanges();
                            lvClients.ItemsSource = clients;
                            lvClients.Items.Refresh();

                            break;
                        case "Client Address":
                            clients = (from c in ctx.Clients.Include("Pets") orderby c.City.ToString() select c).ToList<Client>();
                            ctx.SaveChanges();
                            lvClients.ItemsSource = clients;
                            lvClients.Items.Refresh();
                            break;
                        case "Phone":
                            clients = (from c in ctx.Clients.Include("Pets") orderby c.Phone.ToString() select c).ToList<Client>();
                            ctx.SaveChanges();
                            lvClients.ItemsSource = clients;
                            lvClients.Items.Refresh();
                            break;
                        case "Pets No. &#8595;&#8593;":
                            clients = (from c in ctx.Clients.Include("Pets") orderby c.Pets.Count.ToString()  select c).ToList<Client>();
                            ctx.SaveChanges();
                            lvClients.ItemsSource = clients;
                            lvClients.Items.Refresh();
                            break;

                        default:
                            // code block
                            break;
                    }

                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

    }
}
