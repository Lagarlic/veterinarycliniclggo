﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectVeterinaryClinic
{
    /// <summary>
    /// Interaction logic for PetsDialog.xaml
    /// </summary>
    public partial class PetsDialog : Window
    {
        Client currClient;
        public PetsDialog(Client c)
        {
            currClient = c;
            InitializeComponent();
            comboType.ItemsSource = Enum.GetValues(typeof(PetsTypeEnum)).Cast<PetsTypeEnum>();
            lblClientName.Content = string.Format("{0} {1}", c.FirstName, c.LastName);
            comboType.SelectedIndex = 6;
            updateListView();
        }

        void resetAllFields()
        {
            tbTitle.Text = "";
            tbChipId.Text = "";
            lvPets.SelectedIndex = -1;
            comboType.SelectedIndex = 6;
            dpDateOfBirth.SelectedDate = null;
            imgPicture.Source = null;
            rbGenderNA.IsChecked = true;
            btPrint.IsEnabled = false;
        }

        void updateListView()
        {
            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    var pets = (from p in ctx.Pets where p.ClientId == currClient.ClientId select p).ToList<Pet>();
                    //Globals.Ctx.SaveChanges();
                    lvPets.ItemsSource = pets;
                    lvPets.Items.Refresh();
                    btPrint.IsEnabled = false;
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /********************    BitmapImage <-> byte[]  ********************************************/
        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null) return null;
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }

        private BitmapImage byteArrayToBitmapImage(byte[] byteImage)
        {
            if (byteImage == null) return null;
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = new System.IO.MemoryStream(byteImage);
            image.EndInit();
            return image;
        }
        /************************************************************************************************/

        Pet selectedPet;
        private void lvPets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedPet = (Pet)lvPets.SelectedItem;
            if (selectedPet != null)
            {
                btPrint.IsEnabled = true;
                tbTitle.Text = selectedPet.Title;
                tbChipId.Text = (selectedPet.ChipId != null) ? selectedPet.ChipId.ToString() : "";
                dpDateOfBirth.SelectedDate = selectedPet.DateOfBirth;
                imgPicture.Source = byteArrayToBitmapImage(selectedPet.Photo);
                comboType.SelectedItem = selectedPet.Type;
                if (selectedPet.Gender == GenderEnumP.Male)
                {
                    rbGenderMale.IsChecked = true;
                }
                else if (selectedPet.Gender == GenderEnumP.Female)
                {
                    rbGenderFemale.IsChecked = true;
                }
                else if (selectedPet.Gender == GenderEnumP.NA)
                {
                    rbGenderNA.IsChecked = true;
                }
                else
                { // internal error
                    MessageBox.Show(this, "Error reading gender value", "Data error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
        }

        private void btPicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    imgPicture.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, ex.Message, "Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (UriFormatException ex)
                {
                    MessageBox.Show(this, ex.Message, "Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btDone_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            Close();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string title = tbTitle.Text;
            string chipId = tbChipId.Text;
            DateTime? dateOfBirth = ((dpDateOfBirth.SelectedDate != null) ? dpDateOfBirth.SelectedDate : null);
            GenderEnumP gender;
            if (rbGenderMale.IsChecked == true)
            {
                gender = GenderEnumP.Male;
            }
            else if (rbGenderFemale.IsChecked == true)
            {
                gender = GenderEnumP.Female;
            }
            else if (rbGenderNA.IsChecked == true)
            {
                gender = GenderEnumP.NA;
            }
            else
            { // internal error
                MessageBox.Show(this, "Error reading radio buttons state", "Internal error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            PetsTypeEnum type = (PetsTypeEnum)comboType.SelectedItem;
            byte[] data = bitmapImageToByteArray((BitmapImage)imgPicture.Source);
            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
            {
                ctx.Pets.Add(new Pet
                {
                    ClientId = currClient.ClientId,
                    Title = title,
                    ChipId = chipId,
                    DateOfBirth = dateOfBirth,
                    Gender = gender,
                    Photo = data,
                    Type = type
                }); ;
                ctx.SaveChanges();
            }
            //resetAllFields();               //activate later
            updateListView();
        }

        private void btCleareSelection_Click(object sender, RoutedEventArgs e)
        {
            resetAllFields();
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            selectedPet = (Pet)lvPets.SelectedItem;
            if (selectedPet != null)
            {
                int selectedId = selectedPet.PetId;
                MessageBoxResult result = MessageBox.Show(this, "Do you want to delete selected client?", "Program message", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        try
                        {
                            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                            {
                                Pet petToDelete = (from p in ctx.Pets where p.PetId == selectedId select p).FirstOrDefault<Pet>();
                                if (petToDelete != null)
                                {


                                    ctx.Pets.Remove(petToDelete); // schedule for deletion from database
                                    ctx.SaveChanges();
                                    //MessageBox.Show("Record deleted.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                    updateListView();
                                    resetAllFields();
                                }
                                else
                                {
                                    MessageBox.Show("Record to delete not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                            }
                        }
                        catch (SystemException ex)
                        {
                            MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        Console.WriteLine("This shouldn't happen!");
                        return;
                }
            }
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            selectedPet = (Pet)lvPets.SelectedItem;
            if (selectedPet != null)
            {
                int selectedId = selectedPet.PetId;
                try
                {
                    using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                    {
                        Pet petToUpdate = (from p in ctx.Pets where p.PetId == selectedId select p).FirstOrDefault<Pet>();
                        if (petToUpdate != null)
                        {
                            petToUpdate.Title = tbTitle.Text;
                            petToUpdate.ChipId = tbChipId.Text;
                            petToUpdate.DateOfBirth = ((dpDateOfBirth.SelectedDate != null) ? dpDateOfBirth.SelectedDate : null);
                            if (rbGenderMale.IsChecked == true)
                            {
                                petToUpdate.Gender = GenderEnumP.Male;
                            }
                            else if (rbGenderFemale.IsChecked == true)
                            {
                                petToUpdate.Gender = GenderEnumP.Female;
                            }
                            else if (rbGenderNA.IsChecked == true)
                            {
                                petToUpdate.Gender = GenderEnumP.NA;
                            }
                            else
                            { // internal error
                                MessageBox.Show(this, "Error reading radio buttons state", "Internal error", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                            petToUpdate.Type = (PetsTypeEnum)comboType.SelectedItem;
                            petToUpdate.Photo = bitmapImageToByteArray((BitmapImage)imgPicture.Source);
                            ctx.SaveChanges();
                            //MessageBox.Show("Record updated.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                            updateListView();
                            resetAllFields();
                        }
                        else
                        {
                            MessageBox.Show("Record to update not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)

        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;

            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    switch (headerClicked.Content)
                    {

                        case "Type":
                            var pets = (from c in ctx.Pets where c.ClientId == currClient.ClientId orderby c.Type.ToString() select c).ToList<Pet>();
                            ctx.SaveChanges();
                            lvPets.ItemsSource = pets;
                            lvPets.Items.Refresh();

                            break;
                        case "Gender":
                            pets = (from c in ctx.Pets where c.ClientId == currClient.ClientId orderby c.Gender.ToString() select c).ToList<Pet>();
                            ctx.SaveChanges();
                            lvPets.ItemsSource = pets;
                            lvPets.Items.Refresh();
                            break;
                        case "Chip No.":
                            pets = (from c in ctx.Pets where c.ClientId == currClient.ClientId orderby c.ChipId.ToString() select c).ToList<Pet>();
                            ctx.SaveChanges();
                            lvPets.ItemsSource = pets;
                            lvPets.Items.Refresh();
                            break;
                        case "Title":
                            pets = (from c in ctx.Pets where c.ClientId == currClient.ClientId orderby c.Title.ToString() select c).ToList<Pet>();
                            ctx.SaveChanges();
                            lvPets.ItemsSource = pets;
                            lvPets.Items.Refresh();
                            break;

                        default:
                            // code block
                            break;
                    }

                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            btPrint.IsEnabled = false;

        }

        private void btPrint_Click(object sender, RoutedEventArgs e)
        {
            selectedPet = (Pet)lvPets.SelectedItem;
            if (selectedPet != null)
            {
                PetHistory petHistory = new PetHistory(selectedPet);
                if (petHistory.ShowDialog() == true) { }
            }
        }
    }
}
