﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjectVeterinaryClinic
{
    partial class Employee : IValidatableObject
    {
        static public bool isFirstNameValid(string firstName)
        {
            return !(string.IsNullOrWhiteSpace(firstName) || !Regex.Match(firstName, @"^[A-Za-z0-9 -]{1,25}$").Success);
        }
        static public bool isLastNameValid(string lastName)
        {
            return !(string.IsNullOrWhiteSpace(lastName) || !Regex.Match(lastName, @"^[A-Za-z0-9 -]{1,25}$").Success);
        }
        static public bool isDateOfBirthValid(DateTime dob)
        {
            return !((DateTime.Today.Year - dob.Year) < 18);
        }
        static public bool isPhoneValid(string phone)
        {
            return !(string.IsNullOrWhiteSpace(phone) || !Regex.Match(phone, @"[(]{1}[0-9]{3}[) ]{2}[0-9]{3}[-]{1}[0-9]{4}").Success);
        }
        static public bool isEmailValid(string email)
        {
            return !(string.IsNullOrWhiteSpace(email) || !Regex.Match(email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success);
        }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();

            if (!isFirstNameValid(FirstName))
            {
                results.Add(new ValidationResult("First name must be 1-25 char, upper and lowercase, numbers, '-' and space."));
            }
            if (!isFirstNameValid(LastName))
            {
                results.Add(new ValidationResult("Last name must be 1-25 char, upper and lowercase, numbers, '-' and space."));
            }
            if (!isDateOfBirthValid(DateOfBirth))
            {
                results.Add(new ValidationResult("The client must be 18 y/o or more."));
            }
            if (!isPhoneValid(Phone))
            {
                results.Add(new ValidationResult("Please enter valid phone number."));
            }
            if (!isEmailValid(Email))
            {
                results.Add(new ValidationResult("Please enter valid email."));
            }
            return results;
        }

        public string ToStringForSorting()
        {
            return string.Format("{0}{1}{2}{3}{4}{5}{6}{7}", FirstName, LastName, Street, City, State, Zip, Phone, DateOfBirth);
        }
    }
}
