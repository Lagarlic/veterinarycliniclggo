﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectVeterinaryClinic
{

    public partial class PetHistory : Window
    {
        Pet currentPet;
        Client currentClient;
        public PetHistory(Pet pet)
        {
            currentPet = pet;
            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
            {
                try
                {
                    currentClient = (from c in ctx.Clients where c.ClientId == currentPet.ClientId select c).FirstOrDefault<Client>();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }

            InitializeComponent();
            comboType.ItemsSource = Enum.GetValues(typeof(PetsTypeEnum)).Cast<PetsTypeEnum>();
            
            if (currentClient != null)
            {
                lblClientName.Content = string.Format("{0} {1}", currentClient.FirstName, currentClient.LastName);
            }
            else
            {
                MessageBox.Show("Record to delete not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            tbPhone.Text = currentClient.Phone;
            tbEmail.Text = currentClient.Email;
            tbTitle.Text = currentPet.Title;
            tbChipId.Text = (currentPet.ChipId != null) ? currentPet.ChipId.ToString() : "";
            dpDateOfBirth.SelectedDate = currentPet.DateOfBirth;
            comboType.SelectedItem = currentPet.Type;

            updateListView();

            void updateListView()
            {
                try
                {
                    using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                    {
                        var schedules = (from c in ctx.Schedules.Include("Employee") where c.PetId == currentPet.PetId orderby c.DateTime select c).ToList<Schedule>();
                        ctx.SaveChanges();
                        lvSchedule.ItemsSource = schedules;
                        lvSchedule.Items.Refresh();
                    }
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
           
        }

        private void btPrint_Click(object sender, RoutedEventArgs e)
        {

            PrintDialog printDialog = new PrintDialog();

            if (printDialog.ShowDialog() == true)
            {
                printDialog.PrintVisual(printHistory, "PetHistory");
            }

        }

        private void btDone_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
