﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectVeterinaryClinic
{
    /// <summary>
    /// Interaction logic for ScheduleDialog.xaml
    /// </summary>
    public partial class ScheduleDialog : Window
    {
        public ScheduleDialog()
        {
            InitializeComponent();
            dpDate.SelectedDate = DateTime.Today;
            ListViewCreator();
            updateListView();           
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        List<Schedule> scheduleTemtlate;
        private void ListViewCreator()
        {
            DateTime date = dpDate.SelectedDate.Value.Date;
            TimeSpan interval = new TimeSpan(0, 20, 0);
            scheduleTemtlate = new List<Schedule>();
            TimeSpan startTime = new TimeSpan(9, 0, 0);
            TimeSpan endTime = new TimeSpan(18, 0, 0);
            while (startTime <= endTime)
            {
                DateTime dateTime = date.AddSeconds(startTime.TotalSeconds);
                Schedule schedule = new Schedule { DateTime = dateTime };
                scheduleTemtlate.Add(schedule);
                scheduleTemtlate.Add(schedule);
                startTime = startTime + interval;
            }
            //lvSchedule.ItemsSource = scheduleTemtlate;            
        }

        void updateListView()
        {
            List<Schedule> realDataSchedule = new List<Schedule>();
            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    var schedulesListFromDB = (from s in ctx.Schedules.Include("Employee").Include("Pet").Include("Client") select s).ToList<Schedule>();
                    foreach (Schedule schedule in scheduleTemtlate)
                    {
                        Schedule currS = schedule;
                        foreach (Schedule scheduleDB in schedulesListFromDB)
                        {
                            if (schedule.DateTime == scheduleDB.DateTime && !realDataSchedule.Contains(scheduleDB))
                            {
                                currS = scheduleDB;
                            }
                        }
                        realDataSchedule.Add(currS);
                    }
                    lvSchedule.ItemsSource = realDataSchedule;
                    lvSchedule.Items.Refresh();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void dpDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ListViewCreator();
            updateListView();
            lvSchedule.SelectedIndex = -1;
            btCreateAppointment.IsEnabled = false;
        }


        Schedule selectedSchedule;
        private void btCreateAppointment_Click(object sender, RoutedEventArgs e)
        {
            selectedSchedule = (Schedule)lvSchedule.SelectedItem;
            if (selectedSchedule.ScheduleId > 0)
            {
                return;
            }
            else
            {
                // call wizzard
                WizardScheduleDialog wizardScheduleDialog = new WizardScheduleDialog(selectedSchedule.DateTime);
                if (wizardScheduleDialog.ShowDialog() == true)
                {
                    updateListView();
                }
                 // in any case!!! Not only true (we have a button inside)
            }
        }

        private void lvSchedule_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            selectedSchedule = (Schedule)lvSchedule.SelectedItem;
            if (selectedSchedule.ScheduleId > 0)
            {
                // TODO Modify Schedule Dialog
                UpdateScheduleDialog updateScheduleDialog = new UpdateScheduleDialog(selectedSchedule);
                if (updateScheduleDialog.ShowDialog() == true)
                {
                    updateListView();
                }
            }
            else
            {
                // call wizzard
                WizardScheduleDialog wizardScheduleDialog = new WizardScheduleDialog(selectedSchedule.DateTime);
                if (wizardScheduleDialog.ShowDialog() == true)
                {
                    updateListView();
                }
                //updateListView();// in any case!!! Not only true (we have a button inside)

            }
        }

        private void lvSchedule_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedSchedule = (Schedule)lvSchedule.SelectedItem;
            if (selectedSchedule != null)
            {
                if (selectedSchedule.ScheduleId > 0)
                {
                    btCreateAppointment.IsEnabled = false;
                    return;
                }
                else
                {
                    btCreateAppointment.IsEnabled = true;
                }
            }
        }

        private void lvSchedule_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            selectedSchedule = (Schedule)lvSchedule.SelectedItem;
            if (selectedSchedule.ScheduleId > 0)
            {
                ContextMenu cm = this.FindResource("cmEdit") as ContextMenu;
                cm.PlacementTarget = sender as Button;
                cm.IsOpen = true;
            }
            else
            {
                ContextMenu cm = this.FindResource("cmCreate") as ContextMenu;
                cm.PlacementTarget = sender as Button;
                cm.IsOpen = true;
            }

        }

        private void cmEditmiEdit_Click(object sender, RoutedEventArgs e)
        {
            selectedSchedule = (Schedule)lvSchedule.SelectedItem;
            if (selectedSchedule.ScheduleId > 0)
            {
                UpdateScheduleDialog updateScheduleDialog = new UpdateScheduleDialog(selectedSchedule);
                if (updateScheduleDialog.ShowDialog() == true)
                {
                    updateListView();
                }
            }
        }

        private void cmCreatemiCreate_Click(object sender, RoutedEventArgs e)
        {
            // call wizzard
            WizardScheduleDialog wizardScheduleDialog = new WizardScheduleDialog(selectedSchedule.DateTime);
            if (wizardScheduleDialog.ShowDialog() == true)
            {
                updateListView();
            }          

        }
    }
}
