﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Entity;


namespace ProjectVeterinaryClinic
{
    /// <summary>
    /// Interaction logic for StaffDialog.xaml
    /// </summary>
    public partial class StaffDialog : Window
    {

        WebCam webcam;

        public StaffDialog()
        {
            InitializeComponent(); //TODO disable/enable Buttons
            comboSortBox.SelectedIndex = 0;
            comboType.ItemsSource = Enum.GetValues(typeof(EmployeeTypeE)).Cast<EmployeeTypeE>();
            comboType.SelectedIndex = 4;
            updateListView();
            lblAgeErrorMessage.Visibility = Visibility.Hidden;
            lblFNameErrorMessage.Visibility = Visibility.Hidden;
            lblLEmailErrorMessage.Visibility = Visibility.Hidden;
            lblLNameErrorMessage.Visibility = Visibility.Hidden;
            lblPhoneErrorMessage.Visibility = Visibility.Hidden;
            // camera
            webcam = new WebCam();
            webcam.InitializeWebCam(ref ImgPhoto);
            //--------------------
            btDelete.IsEnabled = false;
            btUpdate.IsEnabled = false;

        }

        void updateListView()
        {
            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    if (comboSortBox.Text == "Last Name")
                    {
                        //var employees = (from c in ctx.Employees.Include("Schedules") select c).ToList<Employee>();
                        var employees = (from c in ctx.Employees orderby c.LastName select c).ToList<Employee>();
                        ctx.SaveChanges();
                        lvEmployees.ItemsSource = employees;
                        lvEmployees.Items.Refresh();
                    }
                    else if (comboSortBox.Text == "Birth Date")
                    {
                        var employees = (from c in ctx.Employees orderby c.DateOfBirth select c).ToList<Employee>();
                        ctx.SaveChanges();
                        lvEmployees.ItemsSource = employees;
                        lvEmployees.Items.Refresh();
                    }

                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void resetAllFields()
        {
            tbFName.Text = "";
            tbLName.Text = "";
            dpDateOfBirth.SelectedDate = null;
            rbGenderNA.IsChecked = true;
            tbPhone.Text = "";
            tbEmail.Text = "";
            tbStreet.Text = "";
            tbCity.Text = "";
            tbState.Text = "";
            tbZip.Text = "";
            ImgPhoto.Source = null;
            comboType.SelectedIndex = 4;
            tbSearch.Text = "";
            btDelete.IsEnabled = false;
            btUpdate.IsEnabled = false;


        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;            
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string firstName = tbFName.Text;
            string lastName = tbLName.Text;
            if (dpDateOfBirth.SelectedDate == null)
            {
                lblAgeErrorMessage.Visibility = Visibility.Visible;
                lblAgeErrorMessage.Content = "Please select the Date.";
                return;
            }
            else
            {
                lblAgeErrorMessage.Visibility = Visibility.Hidden;
            }

            DateTime dateOfBirth = (DateTime)dpDateOfBirth.SelectedDate; // FIXME null point ex if nothing selected
            GenderEnumC gender;
            if (rbGenderMale.IsChecked == true)
            {
                gender = GenderEnumC.Male;
            }
            else if (rbGenderFemale.IsChecked == true)
            {
                gender = GenderEnumC.Female;
            }
            else if (rbGenderNA.IsChecked == true)
            {
                gender = GenderEnumC.NA;
            }
            else
            { // internal error
                MessageBox.Show(this, "Error reading radio buttons state", "Internal error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            EmployeeTypeE type = (EmployeeTypeE)comboType.SelectedItem;
            string phone = tbPhone.Text;
            string email = tbEmail.Text;
            string street = tbStreet.Text;
            string city = tbCity.Text;
            string state = tbState.Text;
            string zip = tbZip.Text;

            byte[] photo = ImageSourceToBytes(new PngBitmapEncoder(), ImgPhoto.Source);

            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
            {
                ctx.Employees.Add(new Employee
                {
                    FirstName = firstName,
                    LastName = lastName,
                    DateOfBirth = dateOfBirth,
                    Gender = gender,
                    Phone = phone,
                    Email = email,
                    Street = street,
                    City = city,
                    State = state,
                    Zip = zip,
                    Photo = photo,
                    EmployeeType = type
                });
                try
                {
                    ctx.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    string errMessage = "";
                    foreach (var error in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in error.ValidationErrors)
                        {
                            errMessage += validationError.ErrorMessage + "\n";
                        }
                    }
                    MessageBox.Show(this, errMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            updateListView();
            resetAllFields();
        }

        Employee selectedEmployee;
        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            selectedEmployee = (Employee)lvEmployees.SelectedItem;
            if (selectedEmployee != null)
            {
                int selectedId = selectedEmployee.EmployeeId;
                MessageBoxResult result = MessageBox.Show(this, "Do you want to delete selected Employee?", "Program message", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:

                        using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                        {

                            try
                            {
                                //Employee employeeToDelete = (from c in Globals.Ctx.Employees where c.EmployeeId == selectedId select c).FirstOrDefault<Employee>();
                                Employee employeeToDelete = (from c in ctx.Employees where c.EmployeeId == selectedId select c).FirstOrDefault<Employee>();
                                if (employeeToDelete != null)
                                {
                                    //Globals.Ctx.Employees.Remove(employeeToDelete); // schedule for deletion from database
                                    ctx.Employees.Remove(employeeToDelete); // schedule for deletion from database
                                    //Globals.Ctx.SaveChanges();
                                    ctx.SaveChanges();
                                    MessageBox.Show("Record deleted.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                    updateListView();
                                    resetAllFields();
                                }
                                else
                                {
                                    MessageBox.Show("Record to delete not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                            }
                            catch (SystemException ex)
                            {
                                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                            break;

                        }



                    case MessageBoxResult.No:
                        break;
                    default:
                        Console.WriteLine("This shouldn't happen!");
                        return;



                }
            }


        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            selectedEmployee = (Employee)lvEmployees.SelectedItem;
            if (selectedEmployee != null)
            {
                int selectedId = selectedEmployee.EmployeeId;
                try
                {
                    using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                    {
                        Employee employeeToUpdate = (from c in ctx.Employees where c.EmployeeId == selectedId select c).FirstOrDefault<Employee>();
                        if (employeeToUpdate != null)
                        {
                            employeeToUpdate.FirstName = tbFName.Text;
                            employeeToUpdate.LastName = tbLName.Text;
                            if (dpDateOfBirth.SelectedDate == null)
                            {
                                lblAgeErrorMessage.Visibility = Visibility.Visible;
                                lblAgeErrorMessage.Content = "Please select the Date.";
                                return;
                            }
                            employeeToUpdate.DateOfBirth = (DateTime)dpDateOfBirth.SelectedDate; // FIXME null point ex if nothing selected 

                            if (rbGenderMale.IsChecked == true)
                            {
                                employeeToUpdate.Gender = GenderEnumC.Male;

                            }
                            else if (rbGenderFemale.IsChecked == true)
                            {
                                employeeToUpdate.Gender = GenderEnumC.Female;

                            }
                            else if (rbGenderNA.IsChecked == true)
                            {
                                employeeToUpdate.Gender = GenderEnumC.NA;

                            }
                            else
                            { // internal error
                                MessageBox.Show(this, "Error reading radio buttons state", "Internal error", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                            employeeToUpdate.Phone = tbPhone.Text;
                            employeeToUpdate.Email = tbEmail.Text;
                            employeeToUpdate.Street = tbStreet.Text;
                            employeeToUpdate.City = tbCity.Text;
                            employeeToUpdate.State = tbState.Text;
                            employeeToUpdate.Zip = tbZip.Text;
                            employeeToUpdate.Photo = ImageSourceToBytes(new PngBitmapEncoder(), ImgPhoto.Source);
                            employeeToUpdate.EmployeeType = (EmployeeTypeE)comboType.SelectedItem;


                            try
                            {
                                ctx.SaveChanges();
                                MessageBox.Show("Record updated.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                updateListView();
                                resetAllFields();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                string errMessage = "";
                                foreach (var error in ex.EntityValidationErrors)
                                {
                                    foreach (var validationError in error.ValidationErrors)
                                    {
                                        errMessage += validationError.ErrorMessage + "\n";
                                    }
                                }
                                MessageBox.Show(this, errMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Record to update not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void lvEmployees_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedEmployee = (Employee)lvEmployees.SelectedItem;
            if (selectedEmployee != null)
            {
                tbFName.Text = selectedEmployee.FirstName;
                tbLName.Text = selectedEmployee.LastName;
                dpDateOfBirth.SelectedDate = selectedEmployee.DateOfBirth;


                if (selectedEmployee.Gender == GenderEnumC.Male)
                {
                    rbGenderMale.IsChecked = true;
                }
                else if (selectedEmployee.Gender == GenderEnumC.Female)
                {
                    rbGenderFemale.IsChecked = true;
                }
                else if (selectedEmployee.Gender == GenderEnumC.NA)
                {
                    rbGenderNA.IsChecked = true;
                }
                else
                { // internal error
                    MessageBox.Show(this, "Error reading gender value", "Data error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }


                tbPhone.Text = selectedEmployee.Phone;
                tbEmail.Text = selectedEmployee.Email;
                tbStreet.Text = selectedEmployee.Street;
                tbCity.Text = selectedEmployee.City;
                tbState.Text = selectedEmployee.State;
                tbZip.Text = selectedEmployee.Zip;
                comboType.SelectedItem = selectedEmployee.EmployeeType;
                btDelete.IsEnabled = true;
                btUpdate.IsEnabled = true;

                if (selectedEmployee.Photo != null)
                {
                    ImgPhoto.Source = ByteToImageSource(selectedEmployee.Photo);
                }
                else
                {
                    ImgPhoto.Source = null;
                }


            }
        }
        //----------------------------------------
        public byte[] ImageSourceToBytes(BitmapEncoder encoder, ImageSource imageSource)
        {
            byte[] bytes = null;
            var bitmapSource = imageSource as BitmapSource;

            if (bitmapSource != null)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }

            return bytes;
        }

        public static ImageSource ByteToImageSource(byte[] imageData)
        {
            BitmapImage biImg = new BitmapImage();
            try
            {
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();

                ImageSource imgSrc = biImg as ImageSource;
                return imgSrc;
            }
            catch (ArgumentNullException ex)
            {                
                MessageBox.Show(ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return null;


        }

        private void btPhoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openPic = new OpenFileDialog();
            openPic.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF";
            if (openPic.ShowDialog() == true)
            {
                Uri imgUri = new Uri(openPic.FileName);

                ImgPhoto.Source = new BitmapImage(imgUri);
            }
            else
            {
                ImgPhoto.Source = null;
            }
        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            this.lvEmployees.SelectedIndex = -1;
            resetAllFields();
        }

        private void ComboSortBox_SelectionChanged(object sender, SelectionChangedEventArgs e)

        {

            ComboBoxItem typeItem = (ComboBoxItem)comboSortBox.SelectedItem;
            string value = typeItem.Content.ToString();

            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    if (value == "Last Name")
                    {
                        //var employees = (from c in ctx.Employees.Include("Schedules") select c).ToList<Employee>();
                        var employees = (from c in ctx.Employees orderby c.LastName select c).ToList<Employee>();
                        ctx.SaveChanges();
                        lvEmployees.ItemsSource = employees;
                        lvEmployees.Items.Refresh();
                    }
                    else if (value == "Birth Date")
                    {
                        var employees = (from c in ctx.Employees orderby c.DateOfBirth select c).ToList<Employee>();
                        ctx.SaveChanges();
                        lvEmployees.ItemsSource = employees;
                        lvEmployees.Items.Refresh();
                    }

                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void TbSearch_TextChanged(object sender, TextChangedEventArgs e)

        {
            string text = tbSearch.Text;

            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    if (text != "")
                    {
                        //var employees = (from c in ctx.Employees.Include("Schedules") select c).ToList<Employee>();
                        //var employees = (from c in ctx.Employees where EF.Functions.Like(c.LastName, "%text%")    select c).ToList<Employee>();
                        var employees = (from emp in ctx.Employees select emp).ToList<Employee>();
                        ctx.SaveChanges();
                        List<Employee> employeesToShow = new List<Employee>();
                        foreach (Employee emp in employees)
                        {
                            if (emp.ToStringForSorting().Contains(text))
                            {
                                employeesToShow.Add(emp);
                            }
                        }
                        lvEmployees.ItemsSource = employeesToShow;
                        lvEmployees.Items.Refresh();
                    }
                    else if (text == "")
                    {
                        updateListView();

                    }

                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }



        }

        // camera block
        private void bntStart_Click(object sender, RoutedEventArgs e)
        {
            webcam.Start();
        }
        private void bntStop_Click(object sender, RoutedEventArgs e)
        {
            webcam.Stop();

            ImgPhoto.Source = ImgPhoto.Source;
        }

        //--------------------------------
        public Employee addEmployee(VeterinaryClinicLgGoEntities1 ctx, string firstName, string lastName, DateTime dateOfBirth, GenderEnumC gender, string phone, string email, string street, string city, string state, string zip, EmployeeTypeE type)
        
        {
            
            {
                var employee =

                ctx.Employees.Add(new Employee
                {
                    FirstName = firstName,
                    LastName = lastName,
                    DateOfBirth = dateOfBirth,
                    Gender = gender,
                    Phone = phone,
                    Email = email,
                    Street = street,
                    City = city,
                    State = state,
                    Zip = zip,
                    //Photo = photo,
                    EmployeeType = type
                });
                try
                {
                    ctx.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    string errMessage = "";
                    foreach (var error in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in error.ValidationErrors)
                        {
                            errMessage += validationError.ErrorMessage + "\n";
                        }
                    }
                    MessageBox.Show(this, errMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);

                }


                return employee;
            }

        }

    }
}
