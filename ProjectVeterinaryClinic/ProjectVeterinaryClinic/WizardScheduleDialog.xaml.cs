﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace ProjectVeterinaryClinic
{
    /// <summary>
    /// Interaction logic for WizardScheduleDialog.xaml
    /// </summary>
    public partial class WizardScheduleDialog : Window
    {
        DateTime scheduleDateTime;
        public WizardScheduleDialog(DateTime selectedDate)
        {
            InitializeComponent();
            comboType.ItemsSource = Enum.GetValues(typeof(PetsTypeEnum)).Cast<PetsTypeEnum>();
            comboTypeSchedule.ItemsSource = Enum.GetValues(typeof(ScheduleStatusEnum)).Cast<ScheduleStatusEnum>();
            comboTypeStaff.ItemsSource = Enum.GetValues(typeof(EmployeeTypeE)).Cast<EmployeeTypeE>();
            comboTypeStaff.SelectedIndex = 4;
            scheduleDateTime = selectedDate;
            comboSortBox.SelectedIndex = 0;
            dpDateTimeOfTask.Value = scheduleDateTime;
            comboTypeSchedule.SelectedItem = ScheduleStatusEnum.Pending;
            dpDateTimeOfTask0.Value = scheduleDateTime;

            updateListView();

        }

        void updateListView()
        {
            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    var clients = (from c in ctx.Clients.Include("Pets") select c).ToList<Client>();
                    lvClients.ItemsSource = clients;
                    lvClients.Items.Refresh();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        void updateListView_Page2()
        {
            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    var pets = (from p in ctx.Pets where p.ClientId == selectedClient.ClientId select p).ToList<Pet>();
                    //Globals.Ctx.SaveChanges();
                    lvPets.ItemsSource = pets;
                    lvPets.Items.Refresh();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        void updateListView_Page3()
        {
            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    if (comboSortBox.Text == "Last Name")
                    {
                        //var employees = (from c in ctx.Employees.Include("Schedules") select c).ToList<Employee>();
                        var employees = (from c in ctx.Employees where c.EmployeeType == EmployeeTypeE.Doctor orderby c.LastName select c).ToList<Employee>();
                        ctx.SaveChanges();
                        lvEmployees.ItemsSource = employees;
                        lvEmployees.Items.Refresh();
                    }
                    else if (comboSortBox.Text == "Birth Date")
                    {
                        var employees = (from c in ctx.Employees where c.EmployeeType == EmployeeTypeE.Doctor orderby c.DateOfBirth select c).ToList<Employee>();
                        ctx.SaveChanges();
                        lvEmployees.ItemsSource = employees;
                        lvEmployees.Items.Refresh();
                    }

                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        Client selectedClient;
        private void lvClients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedClient = (Client)lvClients.SelectedItem;
            if (selectedClient != null)
            {

                lblClientName.Content = string.Format("{0} {1}", selectedClient.FirstName, selectedClient.LastName);

                updateListView_Page2();
                Page1.CanSelectNextPage = true;

            }
        }

        Pet selectedPet;
        private void lvPets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedPet = (Pet)lvPets.SelectedItem;
            if (selectedPet != null)
            {
                tbTitle.Text = selectedPet.Title;
                tbChipId.Text = (selectedPet.ChipId != null) ? selectedPet.ChipId.ToString() : "";
                dpDateOfBirth.SelectedDate = selectedPet.DateOfBirth;
                imgPicture.Source = byteArrayToBitmapImage(selectedPet.Photo);
                comboType.SelectedItem = selectedPet.Type;
                if (selectedPet.Gender == GenderEnumP.Male)
                {
                    rbGenderMale.IsChecked = true;
                }
                else if (selectedPet.Gender == GenderEnumP.Female)
                {
                    rbGenderFemale.IsChecked = true;
                }
                else if (selectedPet.Gender == GenderEnumP.NA)
                {
                    rbGenderNA.IsChecked = true;
                }
                else
                { // internal error
                    MessageBox.Show(this, "Error reading gender value", "Data error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                lblClientNameToGo.Content = string.Format("{0} {1}", selectedClient.FirstName, selectedClient.LastName);
                lblPetNameToGo.Content = string.Format("{0} {1}", selectedPet.Title, selectedPet.ChipId);
                Page2.CanSelectNextPage = true;
                updateListView_Page3();
            }
        }

        Employee selectedEmployee;
        private void lvEmployees_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            selectedEmployee = (Employee)lvEmployees.SelectedItem;
            if (selectedEmployee != null)
            {
                tbFName.Text = selectedEmployee.FirstName;
                tbLName.Text = selectedEmployee.LastName;
                //dpDateOfBirth3.SelectedDate = selectedEmployee.DateOfBirth;


                if (selectedEmployee.Gender == GenderEnumC.Male)
                {
                    rbGenderMale3.IsChecked = true;
                }
                else if (selectedEmployee.Gender == GenderEnumC.Female)
                {
                    rbGenderFemale3.IsChecked = true;
                }
                else if (selectedEmployee.Gender == GenderEnumC.NA)
                {
                    rbGenderNA3.IsChecked = true;
                }
                else
                { // internal error
                    MessageBox.Show(this, "Error reading gender value", "Data error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                tbPhone.Text = selectedEmployee.Phone;
                comboTypeStaff.SelectedItem = selectedEmployee.EmployeeType;
                //tbEmail.Text = selectedEmployee.Email;
                //tbStreet.Text = selectedEmployee.Street;
                //tbCity.Text = selectedEmployee.City;
                //tbState.Text = selectedEmployee.State;
                //tbZip.Text = selectedEmployee.Zip;

                if (selectedEmployee.Photo != null)
                {
                    ImgPhoto.Source = ByteToImageSource(selectedEmployee.Photo);
                }
                else
                {
                    ImgPhoto.Source = null;
                }

                Page3.CanSelectNextPage = true;
                lblDoctorNameToGo.Content = string.Format("{0} {1}", selectedEmployee.FirstName, selectedEmployee.LastName);

            }

        }

        private void ComboSortBox_SelectionChanged(object sender, SelectionChangedEventArgs e)

        {

            ComboBoxItem typeItem = (ComboBoxItem)comboSortBox.SelectedItem;
            string value = typeItem.Content.ToString();

            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    if (value == "Last Name")
                    {
                        //var employees = (from c in ctx.Employees.Include("Schedules") select c).ToList<Employee>();
                        var employees = (from c in ctx.Employees where c.EmployeeType == EmployeeTypeE.Doctor orderby c.LastName select c).ToList<Employee>();
                        ctx.SaveChanges();
                        lvEmployees.ItemsSource = employees;
                        lvEmployees.Items.Refresh();
                    }
                    else if (value == "Birth Date")
                    {
                        var employees = (from c in ctx.Employees where c.EmployeeType == EmployeeTypeE.Doctor orderby c.DateOfBirth select c).ToList<Employee>();
                        ctx.SaveChanges();
                        lvEmployees.ItemsSource = employees;
                        lvEmployees.Items.Refresh();
                    }

                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void TbSearch_TextChanged(object sender, TextChangedEventArgs e)

        {
            string text = tbSearchDoctor.Text;

            try
            {
                using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
                {
                    if (text != "")
                    {
                        //var employees = (from c in ctx.Employees.Include("Schedules") select c).ToList<Employee>();
                        //var employees = (from c in ctx.Employees where EF.Functions.Like(c.LastName, "%text%")    select c).ToList<Employee>();
                        var employees = (from c in ctx.Employees where c.EmployeeType == EmployeeTypeE.Doctor where c.LastName.ToLower().Contains(text.ToLower()) select c).ToList<Employee>();

                        ctx.SaveChanges();
                        lvEmployees.ItemsSource = employees;
                        lvEmployees.Items.Refresh();
                    }
                    else if (text == "")
                    {
                        updateListView_Page3();

                    }

                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }



        }


        /********************    BitmapImage <-> byte[]  ********************************************/
        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null) return null;
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }

        private BitmapImage byteArrayToBitmapImage(byte[] byteImage)
        {
            if (byteImage == null) return null;
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = new System.IO.MemoryStream(byteImage);
            image.EndInit();
            return image;
        }
        /************************************************************************************************/

        private byte[] ImageSourceToBytes(BitmapEncoder encoder, ImageSource imageSource)
        {
            byte[] bytes = null;
            var bitmapSource = imageSource as BitmapSource;

            if (bitmapSource != null)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }

            return bytes;
        }

        private static ImageSource ByteToImageSource(byte[] imageData)
        {
            BitmapImage biImg = new BitmapImage();
            try
            {
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();

                ImageSource imgSrc = biImg as ImageSource;
                return imgSrc;
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show( ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return null;
        }

        //---------------------------------
        private void btAdd_Click(object sender, RoutedEventArgs e)
        {

            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
            {
                ctx.Schedules.Add(new Schedule
                {
                    ClientId = selectedClient.ClientId,
                    PetId = selectedPet.PetId,
                    EmployeeId = selectedEmployee.EmployeeId,
                    Task = tbTask.Text,
                    Status = ScheduleStatusEnum.Pending,
                    DateTime = scheduleDateTime,
                    Note = tbNote.Text
                });
                try
                {
                    ctx.SaveChanges();
                    btSetAppointment.IsEnabled = false;
                }
                catch (DbEntityValidationException ex)
                {
                    string errMessage = "";
                    foreach (var error in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in error.ValidationErrors)
                        {
                            errMessage += validationError.ErrorMessage + "\n";
                        }
                    }
                    MessageBox.Show(this, errMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            this.DialogResult = true;
        }

        private void Wizard_Finish(object sender, Xceed.Wpf.Toolkit.Core.CancelRoutedEventArgs e)
        {
            using (VeterinaryClinicLgGoEntities1 ctx = new VeterinaryClinicLgGoEntities1())
            {
                ctx.Schedules.Add(new Schedule
                {
                    ClientId = selectedClient.ClientId,
                    PetId = selectedPet.PetId,
                    EmployeeId = selectedEmployee.EmployeeId,
                    Task = tbTask.Text,
                    Status = ScheduleStatusEnum.Pending,
                    DateTime = scheduleDateTime,
                    Note = tbNote.Text
                });
                try
                {
                    ctx.SaveChanges();
                    btSetAppointment.IsEnabled = false;
                }
                catch (DbEntityValidationException ex)
                {
                    string errMessage = "";
                    foreach (var error in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in error.ValidationErrors)
                        {
                            errMessage += validationError.ErrorMessage + "\n";
                        }
                    }
                    MessageBox.Show(this, errMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }
    }
}
